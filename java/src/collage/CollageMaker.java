package collage;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import collage.grid.Cell;
import collage.grid.SimpleGridMaker;
import collage.image.FeatureSet;
import collage.image.RescalableImage;
import collage.utils.CellListMerger;
import collage.utils.NearestNeighbour;
import collage.utils.SecondaryImagesConverter;
import collage.utils.SimulatedAnnealing;
//import scala.rectangle.Rectangle;


import scala.tree.*;

//public class CollageMaker {
//
//	static JFrame frame;
//	static JPanel content;
//	static SingleImage img = null;
//	static JMenuItem scaleMenu;
//	
//	public static JMenuBar getMenu() {
//		JMenuBar menuBar = new JMenuBar();
//		
//		JMenu menu = new JMenu("File");
//		menu.setMnemonic(KeyEvent.VK_N);
//		menu.getAccessibleContext().setAccessibleDescription("Access file options.");
//		menuBar.add(menu);
//		
//		JMenuItem menuItem = new JMenuItem("Load source image");
//		menuItem.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				JFileChooser chooser = new JFileChooser();
//				File file = null;
//				int returnValue = chooser.showOpenDialog(null);
//				if (returnValue == JFileChooser.APPROVE_OPTION) {
//					file = chooser.getSelectedFile();
//				}		
//				frame.getContentPane().removeAll();
//				img = new SingleImage(file);
//				scaleMenu.setEnabled(true);
//				frame.getContentPane().add(img);
//				frame.pack();
//				
//				System.out.println(file.getName());
//			}
//			
//		});
//		menu.add(menuItem);
//		
//		menu = new JMenu("Scale");
//		menuBar.add(menu);
//		
//		scaleMenu = new JMenuItem("Scale");
//		scaleMenu.setEnabled(false);
//		scaleMenu.addActionListener(new ActionListener() {
//
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				JTextField wField = new JTextField(4);
//				JTextField hField = new JTextField(4);
//				
//				JPanel panel = new JPanel();
//				panel.add(new JLabel("width scale:"));
//				panel.add(wField);
//				panel.add(Box.createHorizontalStrut(15)); // a spacer
//				panel.add(new JLabel("height scale:"));
//				panel.add(hField);
//				
//				int result = JOptionPane.showConfirmDialog(null, panel, 
//							"Please Enter X and Y Values", JOptionPane.OK_CANCEL_OPTION);
//				if (result == JOptionPane.OK_OPTION) {
//					System.out.println("jee");
//					img.rescale(Double.parseDouble(wField.getText()), Double.parseDouble(hField.getText()));
//				}
//				content.repaint();
//				frame.pack();
//			}
//			
//		});
//		menu.add(scaleMenu);
//		
//		return menuBar;
//	}
//	
//	/**
//	 * @param args
//	 */
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		frame = new JFrame("Collage Maker (tm)");
//		content = new JPanel();
//		
//		frame.setLocationRelativeTo(null);
//		frame.setLayout(new BorderLayout());
//		
//		content.setPreferredSize(new Dimension(600,400));
//		frame.getContentPane().add(content);
//		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		
//		frame.setJMenuBar(getMenu());
//		
//		frame.pack();
//		frame.setVisible(true);
//	}
//
//}


public class CollageMaker {

	static JFrame frame;
	static JPanel content;
	static BufferedImage img = null;
	static JMenuItem scaleMenu;
    static TwoDTree a;
    static TwoDTree b;
    static NeighborGen g;


	
	public static JMenuBar getMenu() {
		JMenuBar menuBar = new JMenuBar();
		
		JMenu menu = new JMenu("File");
		menu.setMnemonic(KeyEvent.VK_N);
		menu.getAccessibleContext().setAccessibleDescription("Access file options.");
		menuBar.add(menu);
		
		JMenuItem menuItem = new JMenuItem("Load source image");
		menuItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				File file = null;
				//file = new File("/home/kom/REPOS/collage/images/sampleA2.jpg");
				//file = new File("/home/kom/REPOS/collage/images/jobs.png");
				
				int returnValue = chooser.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					file = chooser.getSelectedFile();
				}
						
				frame.getContentPane().removeAll();
				try{
					img = ImageIO.read(file);
				} catch (IOException er) {
					// ignore
				}
				//scaleMenu.setEnabled(true);
				//frame.pack();
				
				System.out.println(file.getName());
			}
			
		});
		menu.add(menuItem);
		
		menuItem = new JMenuItem("Load images folder");
		menuItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				File file = null;
				//file = new File("/home/kom/REPOS/collage/images/Bs");
				
				int returnValue = chooser.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					file = chooser.getSelectedFile();
				}
				
				frame.getContentPane().removeAll();
				
				//SimpleGridMaker sgm = new SimpleGridMaker();
				Dimension cellDim = new Dimension(2,2);
				//List<Cell> srcCells = sgm.getGrid(img, new Dimension(40,30), cellDim);
				
				SimulatedAnnealing sa = new SimulatedAnnealing(1000000, 0.00001, 0.99);
				
				SecondaryImagesConverter sic = new SecondaryImagesConverter(file);
				List<FeatureSet> candidates = sic.convertToFeatures(cellDim);
				
				List<Cell> srcCells = sa.perform(img, cellDim, candidates);

				NearestNeighbour nn = new NearestNeighbour();
				FeatureSet nearest;
				List<Cell> newCells = new ArrayList<Cell>();
				
				for (Cell srcCell : srcCells) {
					nearest = nn.find(srcCell.getFeatures(), candidates);
					newCells.add(new Cell(srcCell.getBounds(),nearest));
				}
				
				//CollageImage ci = new CollageImage(newCells,img);
				
				frame.getContentPane().removeAll();
				//frame.getContentPane().add(ci);
				
				CellListMerger clm = new CellListMerger();
				frame.getContentPane().add(new RescalableImage(clm.merge(newCells, new Dimension(img.getWidth(),img.getHeight()))));
				content.repaint();
				frame.pack();
				
				System.out.println(file.getName());
			}
			
		});
		menu.add(menuItem);
		
		return menuBar;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		frame = new JFrame("Collage Maker (tm)");
		content = new JPanel();

//        a = TwoDTree.generateTwoDTree(600,10,400,5,60);
//        g = NeighborGen.apply(10,5,60,1000);
//        b = g.generate(a,600,400,500);
//        for (Rectangle item : b.getRectanglesJava(600,400)){
//            System.out.println(item.getHeight());
//        }
		frame.setLocationRelativeTo(null);
		frame.setLayout(new BorderLayout());
		
		content.setPreferredSize(new Dimension(600,400));

		frame.getContentPane().add(content);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.setJMenuBar(getMenu());
		
		frame.pack();
		frame.setVisible(true);
	}

}

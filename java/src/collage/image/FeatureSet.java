package collage.image;

import java.awt.Color;

/**
 * Holds pixels of a featureset/subimage.
 */
public class FeatureSet {
	
	private String imgName;
	private Color[] colors;
	
	public FeatureSet(String imgName,Color[] colors) {
		this.imgName = imgName;
		this.colors = colors;
	}
	
	public Color[] getColors() {
		return this.colors;
	}
	
	public String getName() {
		return this.imgName;
	}
	
}

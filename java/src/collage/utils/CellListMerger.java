package collage.utils;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import collage.grid.Cell;

/**
 * Merges a list of cells into a single image, hence allowing easy scaling.
 */
public class CellListMerger {

	public BufferedImage merge(List<Cell> cells, Dimension dim) {
		BufferedImage result = new BufferedImage((int)dim.getWidth(),(int)dim.getHeight(),BufferedImage.TYPE_4BYTE_ABGR_PRE);
		Graphics g = result.getGraphics();
		
		// Ripped from Collage image, might want to refactor.
		Map<String,BufferedImage> imageMap = new HashMap<String,BufferedImage>();
		
		File file;
		BufferedImage subImg;
		Rectangle rect;
		
		for (Cell cell : cells) {
			file = new File(cell.getFeatures().getName());
			if (!imageMap.containsValue(file.getName())) {
				subImg = null;
				try{
					subImg = ImageIO.read(file);
				} catch (IOException er) {
					// ignore
				}
				imageMap.put(file.getName(), subImg);
			}
			rect = cell.getBounds();
			subImg = imageMap.get(file.getName());
			g.drawImage(subImg, rect.x, rect.y, rect.width, rect.height, null);
		}
		
		return result;
	}
	
}

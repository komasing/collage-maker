package scala.tree {

import java.awt.Rectangle
import scala.util.Random
import scala.collection.JavaConverters._


abstract class TwoDTree {

  def coordinate : Int
  def dimension : Int

  def getRectangles(sq : Rectangle) : List[Rectangle]

  def getRectangles(x : Int, y : Int) : List[Rectangle] = getRectangles(new Rectangle(0,0,x,y))

  def getRectanglesJava( x : Int, y : Int) : java.util.List[Rectangle]  = getRectangles(x,y).asJava
}

object TwoDTree {
  def apply(coordinate : Int, dimension : Int) : TwoDTree = new Leaf(coordinate,dimension)

  def apply(coordinate : Int, dimension : Int , leftChild : Option[TwoDTree], rightChild : Option[TwoDTree]) : TwoDTree = new Node (coordinate, dimension, leftChild,rightChild)

  def apply(coordinate : Int, dimension : Int, leftChild : TwoDTree, rightChild: TwoDTree) : TwoDTree = this(coordinate,dimension,Some(leftChild),Some(rightChild))

  def defineCoordinateFunction(x : Int, y : Int) : ((Int,Int),Int) => Int = (params,d) => params match {
    case (min,max) => if (d == 1) min + x +  Random.nextInt(max - min- 2*x + 1)  else min + y +  Random.nextInt(max - min - 2*y +1)
  }

  def defineBooleanFunction(x : Int, y : Int, prob : Int) : ((Int,Int),Int) => Boolean = (params,d) => params match {
    case (min,max) => {
      val dif = max - min
      if (d == 1 && dif < 2*x) false
      else if (d == 2 && dif < 2*y ) false
      else Random.nextInt(100) < prob
    }
  }




  def generateTwoDTree(xMin : Int, xMax : Int, yMin : Int, yMax : Int, diam : Int, distCord : ((Int,Int),Int) => Int, distChild : ((Int,Int),Int) => Boolean ) : TwoDTree = {
    def genHelper(dimension : Int, x : (Int,Int), y : (Int,Int) ) : TwoDTree = {

      val d : Int= dimension  % 2 + 1
      val coordinate : Int = if (dimension ==1 ) distCord(x,dimension) else distCord(y,dimension)
      val (xStart,xEnd) = x
      val (yStart,yEnd) = y
      val children : (Boolean,Boolean) = if (dimension == 1) {
        (distChild((xStart,coordinate),dimension),distChild((coordinate,xEnd),dimension))
      }
      else {
        (distChild((yStart,coordinate),dimension),distChild((coordinate,yEnd),dimension))
      }

      children match {

          case (true,true) => {
            if (dimension == 1) {
              TwoDTree(coordinate,dimension,genHelper(d,(xStart,coordinate),y),genHelper(d,(coordinate,xEnd),y))
            } else {
              TwoDTree(coordinate,dimension,genHelper(d,x,(yStart,coordinate)),genHelper(d,x,(coordinate,yEnd)))
            }
          }

          case (true,false) => {
            if (dimension == 1) {
              TwoDTree(coordinate,dimension,Some(genHelper(d,(xStart,coordinate),y)),None)
            } else {
              TwoDTree(coordinate,dimension,Some(genHelper(d,x,(yStart,coordinate))),None)
            }
          }

          case (false,true) => {
            if (dimension == 1) {
              TwoDTree(coordinate,dimension,None,Some(genHelper(d,(coordinate,xEnd),y)))
            }
            else {
              TwoDTree(coordinate,dimension,None,Some(genHelper(d,x,(coordinate,yEnd))))
            }
          }

          case (false,false) => {
            TwoDTree(coordinate,dimension)
          }
        }
      }

    genHelper(diam,(xMin,xMax),(yMin,yMax))
  }

  def generateTwoDTree(xMax : Int, yMax : Int, distCord : ((Int,Int),Int) => Int, distChild : ((Int,Int),Int) => Boolean ) : TwoDTree =  generateTwoDTree(0,xMax,0,yMax,1,distCord,distChild)

  def generateTwoDTree(x : Int, xMin : Int, y : Int, yMin : Int,  prob : Int) : TwoDTree = generateTwoDTree(x,y,defineCoordinateFunction(xMin,yMin),defineBooleanFunction(xMin,yMin,prob))

  def generateTwoDTree(x1 : Int, x2 : Int, xMin : Int, y1 : Int, y2 : Int,  yMin : Int,  prob : Int, d : Int) : TwoDTree = generateTwoDTree(x1,x2,y1,y2,d,defineCoordinateFunction(xMin,yMin),defineBooleanFunction(xMin,yMin,prob))

  def getDivisionToRectangles(x : Int, xMin : Int, y : Int, yMin : Int, prob : Int) : List[Rectangle] = generateTwoDTree(x,xMin,y,yMin,prob).getRectangles(new Rectangle(0,0,x,y))

}

case class Leaf(val coordinate : Int, val dimension : Int) extends TwoDTree {
  def getRectangles(rectangle : Rectangle) : List[Rectangle] =  dimension match {
    case 1 => new Rectangle(rectangle.getX().toInt,rectangle.getY().toInt,(coordinate - rectangle.getX()).toInt,rectangle.getHeight.toInt) :: new Rectangle(coordinate,rectangle.getY().toInt,(rectangle.getMaxX()-coordinate).toInt,rectangle.getHeight().toInt) ::  List()
    case 2 => new Rectangle(rectangle.getX().toInt,rectangle.getY().toInt,rectangle.getWidth().toInt,( coordinate - rectangle.getY()).toInt) :: new Rectangle(rectangle.getX().toInt,coordinate,rectangle.getWidth().toInt,(rectangle.getMaxY() - coordinate).toInt) ::  List()
    case _ => throw new Exception("Shouldn't happen")
  }
}

case class Node (val coordinate : Int, val dimension : Int, val leftChild : Option[TwoDTree], val rightChild : Option[TwoDTree] ) extends TwoDTree {
  def getRectangles(rectangle : Rectangle) : List[Rectangle] = (leftChild,rightChild) match {
    case (None,Some(rightC)) => {
      if (dimension == 1) {
        new Rectangle(rectangle.getX().toInt,rectangle.getY().toInt,(coordinate - rectangle.getX()).toInt,rectangle.getHeight().toInt) :: rightC.getRectangles(new Rectangle(coordinate,rectangle.getY().toInt,(rectangle.getMaxX()-coordinate).toInt,rectangle.getHeight().toInt))
      }
      else {
        new Rectangle(rectangle.getX().toInt,rectangle.getY().toInt,rectangle.getWidth().toInt,(coordinate - rectangle.getY()).toInt) :: rightC.getRectangles(new Rectangle(rectangle.getX().toInt,coordinate,rectangle.getWidth().toInt,(rectangle.getMaxY() - coordinate).toInt))
      }
    }
    case (Some(leftC),None) => {
      if (dimension == 1)  {
        new Rectangle(coordinate,rectangle.getY().toInt,(rectangle.getMaxX()-coordinate).toInt,rectangle.getHeight().toInt) :: leftC.getRectangles(new Rectangle(rectangle.getX().toInt,rectangle.getY().toInt,(coordinate - rectangle.getX()).toInt,rectangle.getHeight().toInt))
      }
      else {
        new Rectangle(rectangle.getX().toInt,coordinate,rectangle.getWidth().toInt,(rectangle.getMaxY() - coordinate).toInt) :: leftC.getRectangles(new Rectangle(rectangle.getX().toInt,rectangle.getY().toInt,rectangle.getWidth().toInt,(coordinate - rectangle.getY()).toInt))
      }
    }
    case (Some(leftC),Some(rightC)) => {
      if (dimension == 1)  {
        leftC.getRectangles(new Rectangle(rectangle.getX().toInt,rectangle.getY().toInt,(coordinate - rectangle.getX()).toInt,rectangle.getHeight().toInt)) ++ rightC.getRectangles(new Rectangle(coordinate,rectangle.getY().toInt,(rectangle.getMaxX()-coordinate).toInt,rectangle.getHeight().toInt))
      }
      else {
        leftC.getRectangles(new Rectangle(rectangle.getX().toInt,rectangle.getY().toInt,rectangle.getWidth().toInt,(coordinate - rectangle.getY()).toInt)) ++ rightC.getRectangles(new Rectangle(rectangle.getX().toInt,coordinate,rectangle.getWidth().toInt,(rectangle.getMaxY() - coordinate).toInt))
      }
    }
    case _ => throw new Exception("The TwoD structure is not valid, a childless Node found")






  }
}

}

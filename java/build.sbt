name := "Collage Maker"

version := "1.0"

mainClass in (Compile, run) := Some( "collage.CollageMaker")

scalaSource in Compile := file("src")

javaSource in Compile := file("src")


package collage.grid;

import java.awt.Rectangle;

import collage.image.FeatureSet;

/**
 * Grid cell that holds its bounds in graphical representation, along with its
 * corresponding features.
 */
public class Cell {

	private Rectangle bounds;
	private FeatureSet features;
	
	public Cell(Rectangle bounds, FeatureSet features) {
		this.bounds = bounds;
		this.features = features;
	}

	public Rectangle getBounds() {
		return bounds;
	}

	public FeatureSet getFeatures() {
		return features;
	}
	
}

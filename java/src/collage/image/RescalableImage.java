package collage.image;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * A rescalable image.
 */
public class RescalableImage extends Component {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BufferedImage img;
	private int w,h;
	private double wScale = 1.0;
	private double hScale = 1.0;
	
	public RescalableImage(File file) {
		try{
			this.img = ImageIO.read(file);
		} catch (IOException er) {
			// ignore
		}
		this.w = this.img.getWidth();
		this.h = this.img.getHeight();
	}
	
	public RescalableImage(BufferedImage img) {
		this.img = img;
		this.w = this.img.getWidth();
		this.h = this.img.getHeight();
	}
	
	public Dimension getPreferredSize() {
		System.out.printf("%d %d", (int)(this.wScale*this.w),(int)(this.hScale*this.h));
        return new Dimension((int)(this.wScale*this.w), (int)(this.hScale*this.h));
    }
	
	public void paint(Graphics g) {
		super.paint(g);
		g.drawImage(this.img, 0, 0, (int)(this.wScale*this.w), (int)(this.hScale*this.h), null);
    }
	
	public BufferedImage getImage() {
		return this.img;
	}
	
	public void rescale(double width, double height) {
		this.wScale = width;
		this.hScale = height;
	}
	
}

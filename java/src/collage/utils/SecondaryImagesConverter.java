package collage.utils;

import java.awt.Dimension;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import collage.grid.Cell;
import collage.image.FeatureSet;

public class SecondaryImagesConverter {

	private File folder;
	
	public SecondaryImagesConverter(File folder) {
		this.folder = folder;
	}
	
	/**
	 * Turns folder content pics into FeatureSets.
	 */
	public List<FeatureSet> convertToFeatures(Dimension cellDim) {
		List<FeatureSet> converted = new ArrayList<FeatureSet>();
		
		String[] files = this.folder.list();
		
		File file;
		FeatureCollector fc = new FeatureCollector();
		for (int i = 0; i < files.length; i++) {
			file = new File(this.folder.getPath(),files[i]);
			System.out.println(file.getName());
			converted.add(fc.gather(file, cellDim));
		}
		
		return converted;
		
	}
	
}

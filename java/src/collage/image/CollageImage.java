package collage.image;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import collage.grid.Cell;

/**
 * Image composed of a list of cells.
 */
public class CollageImage extends Component {

	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private List<Cell> cells;
		private Map<String,BufferedImage> imageMap;
		private BufferedImage srcImg;
		
		public CollageImage(List<Cell> cells, BufferedImage srcImg) {
			this.cells = cells;
			this.srcImg = srcImg;
			this.imageMap = new HashMap<String,BufferedImage>();
		}
		
		public Dimension getPreferredSize() {
	        return new Dimension(this.srcImg.getWidth(),this.srcImg.getHeight());
	    }
		
		public void paint(Graphics g) {
			super.paint(g);
			File file;
			BufferedImage img;
			Rectangle rect;
			for (Cell cell : this.cells) {
				file = new File(cell.getFeatures().getName());
				if (!this.imageMap.containsValue(file.getName())) {
					img = null;
					try{
						img = ImageIO.read(file);
					} catch (IOException er) {
						// ignore
					}
					this.imageMap.put(file.getName(), img);
				}
				rect = cell.getBounds();
				img = this.imageMap.get(file.getName());
				g.drawImage(img, rect.x, rect.y, rect.width, rect.height, null);
			}
	    }
		
}

package collage.utils;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import collage.image.FeatureSet;

public class FeatureCollector {
	
	/**
	 * 
	 * @param img - Image from which we want features.
	 * @param name - Name (path in case of images B_i) of the image.
	 * @param gridDim - Desired dimension of features/subimage
	 * @return FeatureSet
	 */
	public FeatureSet gather(BufferedImage img, String name, Dimension gridDim) {
		
		Color[] colorSet = new Color[gridDim.height*gridDim.width];
		
		int w = img.getWidth();
		int h = img.getHeight();
		
		int wStep = (int)Math.ceil((double)(img.getWidth())/gridDim.width);
		int hStep = (int)Math.ceil((double)(img.getHeight())/gridDim.height);
		
		int xRemaining;
		int yRemaining;
		
		xRemaining = img.getWidth()-1;
		for(int x = 0; x < gridDim.width; x++) {
			yRemaining = img.getHeight()-1;
			for (int y = 0; y < gridDim.height; y++) {
				colorSet[x*(gridDim.width)+y] = getAveragePixel(
						img.getSubimage(x*wStep, y*hStep, Math.min(wStep-1, xRemaining),Math.min(hStep-1, yRemaining)));			
				yRemaining -= (hStep-1);
			}
			xRemaining -= (wStep-1);
		}
		
		return new FeatureSet(name, colorSet);
	}
	
	public FeatureSet gather(File imgFile, Dimension gridDim) {
		BufferedImage img = null;		
		try {
			img = ImageIO.read(imgFile);
		} catch (IOException e) {
			// Ignore
		}
		return this.gather(img, imgFile.getAbsolutePath(), gridDim);
	}
	
	/**
	 * @param image
	 * @return average pixel of image
	 */
	private Color getAveragePixel(BufferedImage image) {
		int w = image.getWidth();
		int h =  image.getHeight();
		int rAcc = 0, gAcc = 0, bAcc = 0;
		int rgb;
		
		for (int x = 0; x < w; x++) {
			for (int y = 0; y < h; y++) {
				rgb = image.getRGB(x, y);
				rAcc += (rgb >> 16) & 0x000000FF;
				gAcc += (rgb >> 8) & 0x000000FF;
				bAcc += rgb & 0x000000FF;
			}
		}
		
		int wh = w*h;
		return new Color(rAcc/wh,gAcc/wh,bAcc/wh);
	}
	
}

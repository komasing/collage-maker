package collage.grid;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import collage.image.FeatureSet;
import collage.utils.FeatureCollector;

/**
 *	Takes an image and a dimension of the grid and returns a list of cells.
 */
public class SimpleGridMaker {
	
	public List<Cell> getGrid(BufferedImage img, Dimension dim, Dimension cellDim) {
		List<Cell> grid = new ArrayList<Cell>();
		FeatureCollector fc = new FeatureCollector();
		
		int w = dim.width;
		int h = dim.height;
		
		int wStep = (int)Math.ceil((double)(img.getWidth())/w);
		int hStep = (int)Math.ceil((double)(img.getHeight())/h);
		
		int xRemaining;
		int yRemaining;
		Rectangle rect;
		FeatureSet fs;
		int img_w = img.getWidth();
		int img_h = img.getHeight();
		
		xRemaining = img.getWidth()-1;
		for (int x = 0; x < img.getWidth(); x += wStep) {
			yRemaining = img.getHeight()-1;
			for (int y = 0; y < img.getHeight(); y += hStep) {
				rect = new Rectangle(x,y,Math.min(wStep, xRemaining),Math.min(hStep, yRemaining));	
				fs = fc.gather(img.getSubimage(x, y, rect.width, rect.height), "", cellDim);
				grid.add(new Cell(rect,fs));
				yRemaining -= hStep;
			}
			xRemaining -= wStep;
		}
		
		return grid;
	}
	
}

package scala.tree

import scala.util.Random

class NeighborGen(xMin : Int, yMin: Int,prob : Int,toChange : (Double,Int) => Boolean) {

  def isSplit(xs : (Int,Int),ys : (Int,Int), d : Int) = if (d == 1) xs match {case (x1,x2) => x2-x1 < 2*xMin} else ys match {case (y1,y2) => y2-y1 < 2*yMin}

  def generate(tree : TwoDTree, x : Int, y : Int, temp : Double) : TwoDTree = {
    def helper(t : TwoDTree, xs : (Int,Int), ys : (Int,Int), level : Int ) : TwoDTree = {
      val (x1,x2) = xs
      val (y1,y2) = ys
      t match {

        case Leaf(cord,d) => if (toChange(temp,level)) TwoDTree.generateTwoDTree(x1,x2,xMin,y1,y2,yMin,prob,d) else TwoDTree(cord,d)

        case Node(c1,d1,None,Some(r)) =>{
          if (toChange(temp,level) && isSplit(xs,ys,d1)){
            TwoDTree.generateTwoDTree(x1,x2,xMin,y1,y2,yMin,prob,d1)
          } else {
            if (d1 == 1) TwoDTree(c1,d1,None,Some(helper(r,(c1,x2),ys,level+1))) else TwoDTree(c1,d1,None,Some(helper(r,xs,(c1,y2),level+1)))
          }
        }

        case Node(c1,d1,Some(l),None) => {
          if (toChange(temp,level) && isSplit(xs,ys,d1)) {
            TwoDTree.generateTwoDTree(x1,x2,xMin,y1,y2,yMin,prob,d1)
          } else {
            if (d1 == 1) TwoDTree(c1,d1,Some(helper(l,(x1,c1),ys,level+1)),None) else TwoDTree(c1,d1,Some(helper(l,xs,(y1,c1),level+1)),None)
          }
        }

        case Node(c1,d1,Some(l),Some(r)) => {
          if (toChange(temp,level) && isSplit(xs,ys,d1)) {

            TwoDTree.generateTwoDTree(x1,x2,xMin,y1,y2,yMin,prob,d1)
          } else {
            if (d1 == 1) TwoDTree(c1,d1,Some(helper(l,(x1,c1),ys,level+1)),Some(helper(r,(c1,x2),ys,level+1))) else TwoDTree(c1,d1,Some(helper(l,xs,(y1,c1),level+1)),Some(helper(r,xs,(c1,y2),level+1)))
          }
        }
      }
    }
    helper(tree,(0,x),(0,y),1)
  }
}

object  NeighborGen {
  def apply(xMin : Int,yMin : Int, prob : Int, t : Int) = new NeighborGen(xMin,yMin,prob,(temp,level) => 50*temp/t * math.pow(2,level-1) > Random.nextDouble()*100)
}

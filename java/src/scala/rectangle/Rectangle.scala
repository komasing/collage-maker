package scala.rectangle{

  class Rectangle(x1 : Int, x2 : Int, y1 : Int,y2 : Int) extends java.awt.Rectangle(x1,y1,x2-x1,y2-y1) {

    def xMin = getX.toInt

    def xMax = getX.toInt + getWidth.toInt

    def yMin = getY.toInt

    def yMax = getY.toInt + getHeight.toInt

    def area() : Double = getWidth()*getHeight()



  }

  object  Rectangle {

    def apply(xMin : Int, xMax : Int, yMin : Int, yMax : Int) : Rectangle = new Rectangle(xMin,xMax,yMin,yMax)

    def apply(x : (Int,Int), y : (Int,Int)) : Rectangle = {
      val (xMin,xMax) = x
      val (yMin,yMax) = y
      this(xMin,xMax,yMin,yMax)
     }
  }
}